const Users = require('./user.model')
const User = {
    list: async(req,res)=>{
        const user = await Users.find();
        console.log(user)
        res.status(200).send(user);

    },
    create: async(req,res)=>{
        const user = new Users(req.body);
        const saveUser = await user.save();
        res.status(201).send(saveUser);
    },
    get: async(req,res)=>{
        const { id } = req.params;
        const user = await Users.findOne({_id: id});
        res.status(200).send(user);
    },
    update:  async(req,res)=>{ 
        //busco registro
        const { id } = req.params;
        const user = await Users.findOne({_id: id});

        //actualizar
        Object.assign(user,req.body);
        await user.save();

        //retornar respuesta
        res.sendStatus(204);
    },
    delete:  async(req,res)=>{ 
        //busco registro
        const { id } = req.params;
        const user = await Users.findOne({_id: id});

        if (user){
            await user.delete();
        }
        
        //retornar respuesta
        res.sendStatus(204);
    },
}
module.exports = User;